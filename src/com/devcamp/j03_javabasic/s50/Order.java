package com.devcamp.j03_javabasic.s50;
import java.util.Date;
import java.util.Locale;

import javax.swing.text.NumberFormatter;

import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


public class Order {
    int id;
    String customerNanme; // tên khách hàng
    long price; // tổng giá tiền 
    Date oderDate; // ngày thực hiện oder 
    boolean confirm; // đã xác nhận hay chưa 
    String [] items;  // danh sách mặt hàng đã mua 
     // khởi tạo tham số  customerNanme-l
    public Order(String customerNanme) {
        this.id = 1;
        this.customerNanme = customerNanme;
        this.price = 120000;
        this.oderDate = new Date();
        this.confirm = true;
        this.items = new String [] {"book", "pen", "rule"};
    }
    // khởi tạo với tất car tham số 

    public Order(int id, String customerNanme, long price, Date oderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerNanme = customerNanme;
        this.price = price;
        this.oderDate = oderDate;
        this.confirm = confirm;
        this.items = new String [] {"book", "pen", "rule"};
    }
    //khởi tạo không tham số 

    public Order() {
        this("2");
    }
    // khởi tạo 3 tham số 
    public Order(int id, String customerNanme, long price) {
        this.id = id;
        this.customerNanme = customerNanme;
        this.price = price;
        this.oderDate = new Date();
        this.confirm = true;
        this.items = new String [] {"book", "pen", "rule"};
    }

    @Override
    public String toString() {
        // đinh nghĩa tiêu chuẩn theo việt nam
        Locale.setDefault(new Locale("vi", "VN"));

        // định dạng theo tháng ngày
        String pattern = "dd-MM-yyyy   HH:mm:ss.SSS";
        DateTimeFormatter defauDateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

        // định dạng cho giá tiền 
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //  trả ra chuỗi string
        return "Order [id=" + id + 
        ", customerNanme=" + customerNanme + 
        ", price=" + usNumberFormat.format(price)  + 
        ", oderDate=" +   defauDateTimeFormatter.format(oderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())+ 
        ", confirm=" + confirm + 
        ", items=" + Arrays.toString(items) + "]";
    }

    
    
    
    

    
    

    
}
