package com.devcamp.j03_javabasic.s50;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import javax.swing.text.NumberFormatter;

import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order2 {
    private int id;
    private String customerName;
    private Integer price = 0;
    private Date orderDate = new Date();
    private Boolean confirm;
    private String[] items;
    private Person buyer;

    public Order2() {
    }

    public Order2(int id) {
        this.id = id;
    }

    public Order2(int id, String customerName, Integer price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
    }

    public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = new Date();
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }

    @Override
    public String toString() {
        // đinh nghĩa tiêu chuẩn theo việt nam
        Locale.setDefault(new Locale("vi", "VN"));

        // định dạng theo tháng ngày
        String pattern = "dd-MM-yyyy   HH:mm:ss.SSS";
        DateTimeFormatter defauDateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

        // định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);

        return "Order2 [id=" + id
                + ", customerName=" + customerName
                + ", price=" + usNumberFormat.format(price)
                + ", orderDate="
                + defauDateTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", confirm=" + confirm +
                ", items=" + Arrays.toString(items)
                + ", buyer=" + buyer + "]";
    }

}
