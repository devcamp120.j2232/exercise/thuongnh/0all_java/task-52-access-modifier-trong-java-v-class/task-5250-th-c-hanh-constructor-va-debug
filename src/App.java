import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j03_javabasic.s50.*;;  // Order



public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<Order> arrOrders = new ArrayList<Order>();
        // khởi tạo 4 object order khác nhau 
        Order order1 = new Order();
        Order order2 = new Order("Lan");
        Order order3 = new Order(3, "long", 80000);
        Order order4 = new Order(4, "name", 75000, new Date(), false, new String[] {"hop mau", "tay", "giay mau"});
        // thêm order object vào danh sách  thay đổi mỗi comm
        arrOrders.add(order1); // cmt
        arrOrders.add(order2);
        arrOrders.add(order3);
        arrOrders.add(order4);
        arrOrders.add(order4);
        // in ra ma hinh 
        for(Order ArrLap: arrOrders){
            System.out.println(ArrLap.toString());
        }
        System.out.println(new Date());
        System.out.println("phần xử lý class oder2 ");

         ArrayList<Order2> arrOrderBs = new ArrayList<Order2>();
        // khởi tạo 4 object order khác nhau 
        Person person1 = new Person();

        Order2 orderB1 = new Order2();
        Order2 orderB2 = new Order2(1000);
        Order2 orderB3 = new Order2(3, "long", 80000);
        Order2 orderB4 = new Order2(4, "name", 75000, new Date(), false, new String[] {"hop mau", "tay", "giay mau"}, person1);
        // thêm order object vào danh sách 
        arrOrderBs.add(orderB1);
        arrOrderBs.add(orderB2);
        arrOrderBs.add(orderB3);
        arrOrderBs.add(orderB4);
        // in ra ma hinh 
        for(Order2 ArrLap2: arrOrderBs){
            System.out.println(ArrLap2.toString());
        }

    }
}
